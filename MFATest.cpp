/**************************************** MFA ******************************************

File name     : MFATest.cpp
Authors       : Kyuseo Park
Version       : 1.0
Last update on: April 16, 2020

Purpose: This file/program carries out a performance test for a DR-Planner which is the MFA. 
It is porting some parts from the original source code to here in order to test only the 
backend part. 
Due to the lack of information of input data, it will only have an input graph which is 
std.skr from the original input files. 

***************************************** MFA *****************************************/
#include<iostream>
#include<fstream>
#include<string>
#include<cmath>
#include <limits.h>
#include <float.h>

int nextEdgeName=1, nextVerName=1, singleVertex=1;

#include "mfa1List.h"
#include "mfa2Clas.h"
#include "mfa3Prnt.cpp"
#include "mfa4Grop.cpp"
#include "mfa5GetC.cpp"
#include "mfa6Dist.cpp"
#include "mfa7DRTree.cpp"
#include "mfa8Algo.cpp"
#include "mfa9Solver.cpp"
#include "mfa10Solver.h"

void getGroups(int *inputData, Graph &graph0, int &idx, List<Cluster> &DR_Trees);
int copyG(Graph &g0, Graph &g1);
List<Cluster> & mfaAlgo(Graph &graph1, List<Cluster> &DRTrees, ostream &file1, ostream &file2);
void resetTrees(Graph &graph0, List<Cluster> &SolverTrees);

int main()
{
	// Get int and double arrays from Java
	// It is a mock-data getting from the Sketcher input std.skr
	// const size_t intLen = 127;
	// const size_t dblLen = 33;
	int dataInt[47] = {0,0,1,0,2,0,3,0,4,-1,0,1,2,1,1,3,1,0,3,2,1,1,2,1,0,4,2,1,1,4,1,0,2,2,2,1,3,1,0,5,2,3,1,4,1,-1,-1};
	 /*{0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,8,0,9,-1,0,4,2,1,1,9,1,0,7,2,1,1,3,1,0,8,2,2,
		1,1,1,0,9,2,2,1,4,1,0,6,2,3,1,2,1,0,10,2,4,1,3,1,0,11,2,4,1,5,1,0,12,2,4,1,6,1,0,13,2,5,1,6,1,
		0,15,2,6,1,7,1,0,14,2,7,1,5,1,0,2,2,8,1,7,1,0,3,2,8,1,1,1,0,5,2,8,1,9,1,0,1,2,9,1,7,1,-1,-1}; */
	double dataDouble[13] = {200.0,86.0,101.0,124.0,194.0,187.0,320.0,123.0,1.0,1.0,1.0,1.0,1.0};
	/*{240.0,93.0,147.0,163.0,209.0,191.0,125.0,251.0,277.0,248.0,274.0,317.0,
		382.0,272.0,298.0,175.0,351.0,138.0,3.0,3.0,3.0,3.0,1.0,3.0,3.0,3.0,1.0,3.0,3.0,3.0,3.0,1.0,3.0}; */
  

  Graph graph0;
  List<Cluster> SolverTrees;
  List<Cluster> DRTrees;

  ofstream outfile1; // output file
  ofstream outfile2; // detail file

  outfile1.open("mfa.out");
  outfile2.open("mfa.dtl");
                        
	int startGroup, startI, startF;
	int type, name, numEnds, theEnd;
	double value;


	int i, j, length;

	Edge newEdge, *theEdge;

	Graph graph1;

	int flag=dataInt[0];
	bool autoS;

	autoS=false;

	cout<<"FLAG: "<<flag<<endl;

	if(flag>10)
	{
	  flag=flag-10;
	  autoS=true;
	}

	switch(flag)
	{
	/*	case 1:		dataInt[0]=2;
		break; */

	case 0:         // Initialize graph0 and DR_Trees based on input file (utu.bin or utu.txt)
	                    startI=1;
		startF=0;
	  			startGroup=graph0.sketchInput(startI, dataInt, startF, dataDouble);

		cout<<"original graph:"<<endl;
	  			//graph0.output(outfile1);

	  			// Initialize the DRTrees structure based on input data
	  			getGroups(dataInt, graph0, startGroup, DRTrees);
		
		cout<<"Groups read"<<endl;

	  			copyG(graph0, graph1);  // copy Graph from graph0 to graph1
	  			graph1.simplify();      // merge multi-edges and remove zero-weight edges

		cout<<"Graph Copied"<<endl;

		graph0.output(cout);

		cout<<"SolverTrees called"<<endl;

	  			// MFA Algorithm: input are graph0, DR_Trees; output is SolverTrees
	  			SolverTrees = mfaAlgo(graph1, DRTrees, outfile1, outfile2);

		fixTree(SolverTrees);

				cout<<"SolverTrees returned"<<endl;

		break;

	case 3:		startI=startF=1;	

		loadState(graph1, graph0, SolverTrees, startI, dataInt, startF, dataDouble);
		
		theEdge=graph0.EdgeAddr(dataInt[startI++]);

		value=dataDouble[startF++];

		cout<<"Value"<<value<<endl;
		
		for(i=startF;i<startF+10;i++)
		   cout<<i<<" "<<dataDouble[i]<<endl;

		theEdge->setValue((float) value);
		
		length=SolverTrees.returnLen();

		for(i=1;i<=length;i++)
		   resetFinByClusterName(SolverTrees.retrieve(i),theEdge->returnEnd1());
	                    for(i=1;i<=length;i++)
		   resetFinByClusterName(SolverTrees.retrieve(i),theEdge->returnEnd2());
		
	                    dataInt[0]=0;
		break;

	case 4: 	cout<<"Start Add"<<endl;
		startI=startF=1;   
		loadState(graph1, graph0, SolverTrees, startI, dataInt, startF, dataDouble);

		cout<<"State Loaded"<<endl;

		type=dataInt[startI++];
		name=dataInt[startI++];
		numEnds=dataInt[startI++];

		newEdge.setName(name);
		newEdge.setType(type);
		newEdge.setWeight(1);			

		for(i=0;i<numEnds;i++)
		{
		   theEnd=dataInt[startI++];
		   newEdge.setEnd(i,theEnd);
		   graph0.VertAddr(theEnd)->appendIncid(name);
		   if(type<=1 || type==4)
		     newEdge.setPart(i, dataInt[startI++]);
		   else newEdge.setPart(i, 0);
		}

		if(type==1 && newEdge.returnPart1()==0 && newEdge.returnPart2()==0)
		  newEdge.setWeight(0);
	                    if(type==1 && newEdge.returnPart1()!=0 && newEdge.returnPart2()!=0)
	                      newEdge.setWeight(2);
		
		if(type==0 || type==4)
		  newEdge.setValue(dataDouble[startF++]);
		else newEdge.setValue(-2.0);

		graph0.appendEdge(newEdge);

		copyG(graph0, graph1);
	                    graph1.simplify();

	                    resetTrees(graph0, DRTrees);

		singleVertex=10;
		
	                    SolverTrees = mfaAlgo(graph1, DRTrees, outfile1, outfile2);

		cout<<"AFTER ADDING CONSTRAINT"<<endl;

		graph0.output(cout);

		print(graph0, SolverTrees);
		
		dataInt[0]=0;

	                    break;	

	case 5:		startI=startF=1;
	                    loadState(graph1, graph0, SolverTrees, startI, dataInt, startF, dataDouble);

		graph0.output(cout);

		print(graph0, DRTrees);

		graph0.delEdgeByName(dataInt[startI++]);

	                    copyG(graph0, graph1);
	                    graph1.simplify();

	                    resetTrees(graph0, DRTrees);

	                    SolverTrees = mfaAlgo(graph1, DRTrees, outfile1, outfile2);

		cout<<"AFTER REMOVING CONSTRAINT"<<endl;

	                    graph0.output(cout);

	                    print(graph0, SolverTrees);

		dataInt[0]=0;		

	                    break;	

	case 7:		startI=startF=1;
			loadState(graph1, graph0, SolverTrees, startI, dataInt, startF, dataDouble);
		
		graph0.sketchInput(startI, dataInt, startF, dataDouble);

		copyG(graph0, graph1); 
	                    graph1.simplify();
	                      
	                    resetTrees(graph0, DRTrees);
	                    
	                    SolverTrees = mfaAlgo(graph1, DRTrees, outfile1, outfile2);

		cout<<"AFTER ADDING OBJECT & CONSTRAINT"<<endl;

	                    graph0.output(cout);

	                    print(graph0, SolverTrees);

	                    dataInt[0]=0;
	                    
	                    break;
	}	

	if(autoS) dataInt[0]=1;
	
	// Solver subroutine
  Solver(graph1, graph0, SolverTrees, dataInt, dataDouble);
  checkEdge(graph0.returnEdgeByName(1), graph0, SolverTrees);
}