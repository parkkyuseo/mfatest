#
# Makefile for testing MFA
#

GMPSRC     = -g
#GMPSRC    = -g -ggdb
GPP        = g++-9 -std=c++17 $(GMPSRC)
GMPLIB     =
#GMPLIB    = -lgmpxx -lgmp

OBJECTS = MFATest.o \
#				  mfa3Prnt.o \
#				  mfa4Grop.o \
#				  mfa5GetC.o \
#				  mfa6Dist.o \
#				  mfa7DRTree.o \
#				  mfa8Algo.o \
 #         mfa9Solver.o 
          
MFATest: $(OBJECTS)
	$(GPP) -o $@ $^ $(GMPLIB)

MFATest.o: MFATest.cpp \
	mfa1List.h \
	mfa2Clas.h \
	mfa10Solver.h
	$(GPP) -o $@ -c $<

mfa1List.o: #mfa1List.cpp \
	mfa1List.h \
#	mfa2Clas.h
	$(GPP) -o $@ -c $<

mfa2Clas.o: #mfa2Clas.cpp \
	mfa2Clas.h \
#	mfa1List.h
	$(GPP) -o $@ -c $<

mfa3Prnt.o: mfa3Prnt.cpp \
#	mfa1List.h \
#	mfa2Clas.h
	$(GPP) -o $@ -c $<

mfa4Grop.o: mfa4Grop.cpp \
#	mfa1List.h \
#	mfa2Clas.h
	$(GPP) -o $@ -c $<

mfa5GetC.o: mfa5GetC.cpp \
#	mfa1List.h \
#	mfa2Clas.h
	$(GPP) -o $@ -c $<

mfa6Dist.o: mfa6Dist.cpp \
#	mfa1List.h \
#	mfa2Clas.h
	$(GPP) -o $@ -c $<

mfa7DRTree.o: mfa7DRTree.cpp \
#	mfa1List.h \
#	mfa2Clas.h
	$(GPP) -o $@ -c $<

mfa8Algo.o: mfa8Algo.cpp \
#	mfa1List.h \
#	mfa2Clas.h
	$(GPP) -o $@ -c $<

mfa9Solver.o: mfa9Solver.cpp \
	mfa10Solver.h
	$(GPP) -o $@ -c $<









clean:
	rm -f $(wildcard *.o) $(wildcard *.csv) MFATest

